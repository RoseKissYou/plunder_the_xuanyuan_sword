// CluDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Editor.h"
#include "CluDlg.h"
#include "Scene.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCluDlg dialog


CCluDlg::CCluDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCluDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCluDlg)
	m_nIndex = -1;
	m_nPara1 = 0;
	m_nPara2 = 0;
	m_nPara3 = 0;
	m_nPara4 = 0;
	m_strPara1 = _T("");
	m_strPara2 = _T("");
	m_strPara3 = _T("");
	m_strPara4 = _T("");
	//}}AFX_DATA_INIT
}


void CCluDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCluDlg)
	DDX_Control(pDX, IDC_EDIT4, m_editPara4);
	DDX_Control(pDX, IDC_EDIT3, m_editPara3);
	DDX_Control(pDX, IDC_EDIT2, m_editPara2);
	DDX_Control(pDX, IDC_EDIT1, m_editPara1);
	DDX_Control(pDX, IDC_COMBO, m_comboCommand);
	DDX_CBIndex(pDX, IDC_COMBO, m_nIndex);
	DDX_Text(pDX, IDC_EDIT1, m_nPara1);
	DDX_Text(pDX, IDC_EDIT2, m_nPara2);
	DDX_Text(pDX, IDC_EDIT3, m_nPara3);
	DDX_Text(pDX, IDC_EDIT4, m_nPara4);
	DDX_Text(pDX, IDC_TEXT1, m_strPara1);
	DDX_Text(pDX, IDC_TEXT2, m_strPara2);
	DDX_Text(pDX, IDC_TEXT3, m_strPara3);
	DDX_Text(pDX, IDC_TEXT4, m_strPara4);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCluDlg, CDialog)
	//{{AFX_MSG_MAP(CCluDlg)
	ON_CBN_SELCHANGE(IDC_COMBO, OnSelchangeCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCluDlg message handlers

int CCluDlg::DoModal(CLU* pClu) 
{
	// TODO: Add your specialized code here and/or call the base class
	m_pClu = pClu;
	//初始化
	m_nIndex = pClu->nCluType-1;
	m_nPara1 = pClu->nPara1;
	m_nPara2 = pClu->nPara2;
	m_nPara3 = pClu->nPara3;
	m_nPara4 = pClu->nPara4;
	//初始化结束
	return CDialog::DoModal();
}

void CCluDlg::UpdatePrompt()
{
	UpdateData(TRUE);
	//
	switch( m_nIndex )
	{
	case 0:
	case 12:
		m_strPara1 = "源ID号";
		m_strPara2 = "目标ID号";
		m_strPara3 = "";
		m_strPara4 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_SHOW);
		m_editPara3.ShowWindow(SW_HIDE);
		m_editPara4.ShowWindow(SW_HIDE);
		break;
	case 1:
		m_strPara1 = "立即数";
		m_strPara2 = "目标ID号";
		m_strPara3 = "";
		m_strPara4 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_SHOW);
		m_editPara3.ShowWindow(SW_HIDE);
		m_editPara4.ShowWindow(SW_HIDE);
		break;
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 10:
	case 11:
		m_strPara1 = "第一个运算量的ID号";
		m_strPara2 = "第二个运算量的ID号";
		m_strPara3 = "结果的ID号";
		m_strPara4 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_SHOW);
		m_editPara3.ShowWindow(SW_SHOW);
		m_editPara4.ShowWindow(SW_HIDE);
		break;
	case 7:
	case 8:
	case 9:
		m_strPara1 = "第一个比较量的ID号";
		m_strPara2 = "第二个比较量的ID号";
		m_strPara3 = "源ID号";
		m_strPara4 = "目标ID号";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_SHOW);
		m_editPara3.ShowWindow(SW_SHOW);
		m_editPara4.ShowWindow(SW_SHOW);
		break;
	}
	//
	UpdateData(FALSE);
}

BOOL CCluDlg::CheckValue()
{
	UpdateData(TRUE);
	//
	if( m_nIndex == 1 )	//立即数赋值指令
	{
		if( m_nPara1<-32768 || m_nPara1>32768 )
		{
			MessageBox("参数1的取值范围是-32768到32768！",NULL,MB_OK);
			return FALSE;
		}
	}
	else
	{
		if( m_nPara1<1 || m_nPara1>256 )
		{
			MessageBox("参数1的取值范围是1到256！",NULL,MB_OK);
			return FALSE;
		}
	}
	//
	if( m_nPara2<1 || m_nPara2>256 )
	{
		MessageBox("参数2的取值范围是1到256！",NULL,MB_OK);
		return FALSE;
	}
	//
	if( m_nIndex>1 && m_nIndex<12 )
	{
		if( m_nPara3<1 || m_nPara3>256 )
		{
			MessageBox("参数3的取值范围是1到256！",NULL,MB_OK);
			return FALSE;
		}
	}
	//
	if( m_nIndex>6 && m_nIndex<10 )
	{
		if( m_nPara4<1 || m_nPara4>256 )
		{
			MessageBox("参数4的取值范围是1到256！",NULL,MB_OK);
			return FALSE;
		}
	}
	//
	return TRUE;
}

BOOL CCluDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	UpdatePrompt();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCluDlg::OnSelchangeCombo() 
{
	// TODO: Add your control notification handler code here
	UpdatePrompt();
}

void CCluDlg::OnOK() 
{
	// TODO: Add extra validation here
	if( CheckValue() )
	{
		//保存数据
		m_pClu->nCluType = (m_nIndex+1);
		m_pClu->nPara1   = m_nPara1;
		m_pClu->nPara2   = m_nPara2;
		m_pClu->nPara3   = m_nPara3;
		m_pClu->nPara4   = m_nPara4;
	}
	else
	{
		return;
	}
	CDialog::OnOK();
}
