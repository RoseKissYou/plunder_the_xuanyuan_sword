// mapeditDoc.cpp : implementation of the CMapeditDoc class
//

#include "stdafx.h"
#include "mapedit.h"
#include "mapeditDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapeditDoc

IMPLEMENT_DYNCREATE(CMapeditDoc, CDocument)

BEGIN_MESSAGE_MAP(CMapeditDoc, CDocument)
	//{{AFX_MSG_MAP(CMapeditDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CMapeditDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CMapeditDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IMapedit to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {CB086EB3-BCCC-4EDA-BDC0-AEB0AAD151C9}
static const IID IID_IMapedit =
{ 0xcb086eb3, 0xbccc, 0x4eda, { 0xbd, 0xc0, 0xae, 0xb0, 0xaa, 0xd1, 0x51, 0xc9 } };

BEGIN_INTERFACE_MAP(CMapeditDoc, CDocument)
	INTERFACE_PART(CMapeditDoc, IID_IMapedit, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMapeditDoc construction/destruction

CMapeditDoc::CMapeditDoc()
{
	// TODO: add one-time construction code here
	EnableAutomation();
	AfxOleLockApp();
}

CMapeditDoc::~CMapeditDoc()
{
	//
	AfxOleUnlockApp();
}

BOOL CMapeditDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	ZeroMemory(map, sizeof(map));
	ZeroMemory(evt, sizeof(evt));

	m_sEventFileName = "";
	m_bEventModified = FALSE;
	return TRUE;
}

BOOL CMapeditDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	// 修改标题栏显示的文件名
	m_sMapFName = GetFileName(CString(lpszPathName));
	SetTitle(m_sMapFName);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
int CMapeditDoc::ReadMap(int x,int y)
{//从地图矩阵x,y的位置读出一个数值，作为函数的返回值返回
	if((x>MAP_GRID_W-1)||(x<0)||(y>MAP_GRID_H-1)||(y<0)){
		MessageBox(NULL,"(Error-001)读取地图文件时输入的位置编号超出了地图尺寸",NULL,MB_OK);
		PostQuitMessage(0);
	}
	int z=(((int)map[x][y][0]*100)+(int)(map[x][y][1]));
	return(z);
}

void CMapeditDoc::WriteMap(int x,int y,int z)
{//向地图矩阵x,y的位置写入z
	if ((x>MAP_GRID_W-1) ||
		(x<0) ||
		(y>MAP_GRID_H-1) ||
		(y<0) ||
		(z>MAP_TILES_NUM_MAX) ||
		(z<0)) {
		MessageBox(NULL,"(Error-002)写入地图文件时输入的位置编号超出了地图尺寸",NULL,MB_OK);
		PostQuitMessage(0);
	}
	//旧数据写入undo序列
	save((ReadMap(x,y)),x,y);

	if(z!=0){
		map[x][y][0]=(char)(z / 100);
		map[x][y][1]=(char)(z % 100);
	}
	else{
		map[x][y][0]=0;
		map[x][y][1]=0;
	}
	SetModifiedFlag();
}

int CMapeditDoc::ReadEvt(int x, int y)
{//读事件，从事件矩阵x,y的位置读出一个数值
	if((x>MAP_GRID_W-1)||(x<0)||(y>MAP_GRID_H-1)||(y<0)){
		MessageBox(NULL,"(Error-003)读取事件文件时输入的位置编号超出了地图尺寸",NULL,MB_OK);
		PostQuitMessage(0);
	}
	int z=(((int)evt[x][y][0]*100)+(int)(evt[x][y][1]));
	return(z);
}

void CMapeditDoc::WriteEvt(int x, int y, int z)
{//写事件，向事件矩阵x,y的位置写入z
	if ((x>MAP_GRID_W-1) ||
		(x<0) ||
		(y>MAP_GRID_H-1) ||
		(y<0) ||
		(z>MAP_TILES_NUM_MAX) ||
		(z<0)) {
		MessageBox(NULL,"(Error-004)写入事件文件时输入的位置编号超出了地图尺寸",NULL,MB_OK);
		PostQuitMessage(0);
	}
	//事件编辑不能undo
	if(z!=0){
		evt[x][y][0]=(char)(z / 100);
		evt[x][y][1]=(char)(z % 100);
	}
	else{
		evt[x][y][0]=0;
		evt[x][y][1]=0;
	}
	SetModifiedFlag();
	m_bEventModified = TRUE;
}
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMapeditDoc serialization

void CMapeditDoc::Serialize(CArchive& ar)
{//存取文件
	int i, j;

	ResetBackup();

	if (ar.IsStoring())
	{//存盘
		// TODO: add storing code here
		for (i=0;i<MAP_GRID_H;i++) {
			for (j=0;j<MAP_GRID_W;j++) {
				ar<<map[i][j][0]<<map[i][j][1];
			}
		}

		//如果指定了事件文件，则还要存入事件文件
		if (! m_sEventFileName.IsEmpty()) {
			SaveEventFile();
		}
	}
	else
	{//读盘
		// TODO: add loading code here
		BOOL bValueOutOfRange = FALSE;
		for (i=0;i<MAP_GRID_H;i++) {
			for (j=0;j<MAP_GRID_W;j++) {
				ar>>map[i][j][0]>>map[i][j][1];
				int v=(((int)map[i][j][0]*100)+(int)(map[i][j][1]));
				if (v<MAP_TILES_ZERO || v>MAP_TILES_NUM_MAX) {
					map[i][j][0] = 0;
					map[i][j][1] = 0;
					bValueOutOfRange = TRUE;
				}
			}
		}

		if (bValueOutOfRange) {
			MessageBox(NULL, "地图文件中存在非法值，已用零值填充！", "提示", MB_OK);
		}

		//清空事件文件
		ZeroMemory(evt, sizeof(evt));
		m_sEventFileName = "";
		m_bEventModified = FALSE;
	}
}

void CMapeditDoc::SaveEventFile(void)
{
	int i, j;
	CFile file(m_sEventFileName, CFile::modeCreate | CFile::modeWrite);
	CArchive ar(&file, CArchive::store);
	for (i=0;i<MAP_GRID_H;i++) {
		for (j=0;j<MAP_GRID_W;j++) {
			ar<<evt[i][j][0]<<evt[i][j][1];
		}
	}
	m_bEventModified = FALSE;
}
void CMapeditDoc::LoadEventFile(void)
{
	int i, j;
	CFile file(m_sEventFileName, CFile::modeRead);
	CArchive ar(&file, CArchive::load);

	BOOL bValueOutOfRange = FALSE;
	for (i=0;i<MAP_GRID_H;i++) {
		for (j=0;j<MAP_GRID_W;j++) {
			ar>>evt[i][j][0]>>evt[i][j][1];
			int v=(((int)evt[i][j][0]*100)+(int)(evt[i][j][1]));
			if (v<MAP_TILES_ZERO || v>MAP_TILES_NUM_MAX) {
				evt[i][j][0] = 0;
				evt[i][j][1] = 0;
				bValueOutOfRange = TRUE;
			}
		}
	}

	if (bValueOutOfRange) {
		MessageBox(NULL, "事件文件中存在非法值，已用零值填充！", "提示", MB_OK);
	}

	m_bEventModified = FALSE;

	CString sEventFName = GetFileName(m_sEventFileName);
	SetTitle(m_sMapFName + "；" + sEventFName);
}
/////////////////////////////////////////////////////////////////////////////
// CMapeditDoc diagnostics

#ifdef _DEBUG
void CMapeditDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMapeditDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMapeditDoc commands
//███████████████████████████████████████
void CMapeditDoc::ResetBackup()
{//复位剪切板队列指针
	m_bkup_counter=0;
	m_bkup_top=0;
}
//////////////////////////////////////////////////////////////////////
void CMapeditDoc::save(int value,int m_x_value,int m_y_value)
{//将当前操作写入undo队列。是Write函数的子函数
	m_bkup_counter=((m_bkup_counter==CAPACITY)?m_bkup_counter:(++m_bkup_counter));//counter最大值等于CAPACITY
	m_bkup_top=((m_bkup_top==(CAPACITY-1))?0:(++m_bkup_top));
	backup[m_bkup_top][0]=value;//写入用于undo操作的队列
	backup[m_bkup_top][1]=m_x_value;
	backup[m_bkup_top][2]=m_y_value;
}
//////////////////////////////////////////////////////////////////////
BOOL CMapeditDoc::Load(int *pvalue,int *pm_x_value,int *pm_y_value)
{//从undo队列中读出一个数据。如果读出失败，将返回FALSE。是Undo函数的子函数
	if(m_bkup_counter>0){
		*pvalue=backup[m_bkup_top][0];
		*pm_x_value=backup[m_bkup_top][1];
		*pm_y_value=backup[m_bkup_top][2];
		m_bkup_top=((m_bkup_top==0)?(CAPACITY-1):(--m_bkup_top));
		m_bkup_counter--;
		if(m_bkup_counter==0){
			undo_st=FALSE;
		}
		else{
			undo_st=TRUE;
		}
		return TRUE;
	}
	else{
		return FALSE;
	}
}
//////////////////////////////////////////////////////////////////////
void CMapeditDoc::Undo(void)
{//执行undo操作
	int z;//地图单元格数值
	int x;//反写入的坐标
	int y;
	if (Load(&z, &x, &y)) {
		if (z != 0) {
			map[x][y][0]=(char)(z/100);//将undo队列读出的内容写回地图
			map[x][y][1]=(char)(z%100);
		}
		else{
			map[x][y][0]=0;//将0写入地图
			map[x][y][1]=0;
		}
	}
}
//███████████████████████████████████████
