void CMainFrame::Para6Init(void)
{
/////////////////////////////////////////////////////////////////////////////
//调入所需对话，主角图片，地图文件群。
	LoadDialog("dlg\\para6.dlg");//调入对话
	LoadActor("pic\\hero.bmp");//调入主角
	deleteallbmpobjects();
/////////////////////////////////////////////////////////////////////////////
//调入所需头像
	hbmp_Photo0=(HBITMAP)LoadImage(NULL,"pic\\pic300.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_Photo1=(HBITMAP)LoadImage(NULL,"pic\\pic302.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic301.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic322.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//西蒙
	hbmp_Photo4=(HBITMAP)LoadImage(NULL,"pic\\pic315.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（1）
	hbmp_Photo5=(HBITMAP)LoadImage(NULL,"pic\\pic309.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//哈德尔
	hbmp_Photo6=(HBITMAP)LoadImage(NULL,"pic\\pic310.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（1）
	hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic313.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（4）
	hbmp_Photo8=(HBITMAP)LoadImage(NULL,"pic\\pic323.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡洛德
	hbmp_Photo9=(HBITMAP)LoadImage(NULL,"pic\\pic303.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//教徒
/////////////////////////////////////////////////////////////////////////////
//调入所需道具
	hbmp_Prop0=(HBITMAP)LoadImage(NULL,"pic\\pic204.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//手枪
	hbmp_Prop1=(HBITMAP)LoadImage(NULL,"pic\\pic211.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//西科斯基
	hbmp_Prop2=(HBITMAP)LoadImage(NULL,"pic\\pic212.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//图波列夫
	hbmp_Prop3=(HBITMAP)LoadImage(NULL,"pic\\pic213.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//苏霍伊
	hbmp_Prop4=(HBITMAP)LoadImage(NULL,"pic\\pic214.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//安东诺夫
	hbmp_Prop5=(HBITMAP)LoadImage(NULL,"pic\\pic215.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//雅克夫列夫
	hbmp_Prop6=(HBITMAP)LoadImage(NULL,"pic\\pic216.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//米高扬
	hbmp_Prop7=(HBITMAP)LoadImage(NULL,"pic\\pic217.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//格列维奇
	hbmp_Prop8=(HBITMAP)LoadImage(NULL,"pic\\pic218.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//拉夫契金
/////////////////////////////////////////////////////////////////////////////
//调入其他图片
	hbmp_0=(HBITMAP)LoadImage(NULL,"pic\\hero.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺
	hbmp_1=(HBITMAP)LoadImage(NULL,"pic\\pic107.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜
	hbmp_2=(HBITMAP)LoadImage(NULL,"pic\\pic108.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//老头
	hbmp_3=(HBITMAP)LoadImage(NULL,"pic\\pic109.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//哈德尔
	hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic007.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//爆炸
	hbmp_5=(HBITMAP)LoadImage(NULL,"pic\\pic114.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//圣剑单图
	hbmp_6=(HBITMAP)LoadImage(NULL,"pic\\pic210.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//汗叹倒
/////////////////////////////////////////////////////////////////////////////
//引入开始游戏的位置
/////////////////////////////////////////////////////////////////////////////
}//OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106a.npc");//调入地图的语句
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para6Info(int type)
{
	int i;

if(st_dialog==20)
{//行走状态的陷阱。
	m_info_st=8;
	m_oldinfo_st=8;
	SetActTimer();
	st_dialog=2000;
}
else if(st_dialog==21)
{//战斗状态的陷阱。
	if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
		m_info_st=100;m_oldinfo_st=100;
	}else{
		m_info_st=10;m_oldinfo_st=10;
	}
	SetActTimer();
	st_dialog=2000;
}
else if(st_sub1==0){
	if(st_dialog==0)
	{
		PlayMidi("midi\\p1015.mid");
		OpenMap("map\\maps103.bmp","map\\map103f.map","map\\map103g.npc");
		current.x=56;
		current.y=93;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(320,384,32,48,96,48,0,48,&BuffDC);
		MemDC.SelectObject(hbmp_1);//荻娜
		resetblt();
		blt(384,304,32,48,96,144,0,144,&BuffDC);
		vopen();
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		for(i=0;i<22;i++){//移动
			blt((808-(i*16)),304,32,48,160,48,64,48,pdc);
			GameSleep(50);
			blt((800-(i*16)),304,32,48,128,48,32,48,pdc);
			GameSleep(50);
		}
		blt(448,304,32,48,96,48,0,48,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,16,0,1);
		st_dialog=1;
	}
	else if(st_dialog==1){
		Dialog(4,21,0,0,2);
		st_dialog=2;
	}
	else if(st_dialog==2){
		Dialog(4,0,16,0,3);
		st_dialog=3;
	}
	else if(st_dialog==3){//哈德尔走来
		MemDC.SelectObject(hbmp_3);//哈德尔
		CDC *pdc=GetDC();
		resetblt();
		for(i=0;i<18;i++){//移动
			blt((i*16),384,32,48,160,144,64,144,pdc);
			GameSleep(50);
			blt((8+(i*16)),384,32,48,128,144,32,144,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,26,0,0,4);
		st_dialog=4;
	}
	else if(st_dialog==4){
		lclose();
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=287;///////////////
		current.x=60;
		current.y=95;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_RIGHT;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		lopen();
		Dialog(2,0,0,0,5);
		st_dialog=5;
	}
	else if(st_dialog==5){
		Dialog(2,0,0,0,6);
		st_dialog=6;
	}
	else if(st_dialog==6){
		Dialog(2,0,0,0,7);
		st_dialog=7;
	}
	else if(st_dialog==7){
		levelup();
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		SetActTimer();
		st_dialog=2000;
	}
	else if(st_dialog==30){
		Dialog(4,0,16,0,56);
		st_dialog=21;
	}
	else if(st_dialog==31){
		current.y+=3;
		oldcurrent.y=current.y;
		Dialog(4,0,32,0,58);
		st_dialog=21;
	}
	else if(st_dialog==32){
		Dialog(4,0,33,0,11);
		st_dialog=33;
	}
	else if(st_dialog==33){
		Dialog(1,0,0,0,12);
		st_dialog=34;
	}
	else if(st_dialog==34){
		Dialog(1,0,0,0,13);
		st_dialog=35;
	}
	else if(st_dialog==35){
		Dialog(4,0,32,0,14);
		st_dialog=20;
	}
}//st_sub1=0的部分结束
else if(st_sub1==1){
	if(st_dialog==0){
		st_dialog=1;
		Dialog(4,0,31,0,16);
	}
	else if(st_dialog==1){
		PlayMidi("midi\\p1014.mid");
		OpenMap("map\\maps101.bmp","map\\map101a.map","map\\map101b.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=384;///////////////
		current.x=30;
		current.y=119;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		lopen();
		if(m_info_prop4&&bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		CDC *pdc=GetDC();
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		SetActTimer();
		st_dialog=2000;
	}
	else if(st_dialog==30){//断桥处跟士兵的对话
		Dialog(4,26,0,0,18);
		st_dialog=32;
	}
	else if(st_dialog==32){
		Dialog(4,0,0,0,19);
		st_dialog=33;
	}
	else if(st_dialog==33){//满血
		PlayWave("snd//levelup.wav");
		blood=200;
		CDC *pdc=GetDC();
		RenewInfo(pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(2,0,0,0,20);
		st_dialog=34;
	}
	else if(st_dialog==34){
		Dialog(4,0,0,0,21);
		st_dialog=21;
	}
	else if(st_dialog==40){//到达防线
		m_info_st=9;
		m_oldinfo_st=9;
		//西蒙，哈德尔冲入防线，卡诺堵住入口，荻娜回头。
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		for(i=0;i<16;i++){//移动
			blt(384,(256+(i*16)),32,48,160,0,64,0,pdc);
			GameSleep(50);
			blt(384,(264+(i*16)),32,48,128,0,32,0,pdc);
			GameSleep(50);
		}
		MemDC.SelectObject(hbmp_3);//哈德尔
		resetblt();
		for(i=0;i<16;i++){//移动
			blt(384,(256+(i*16)),32,48,160,0,64,0,pdc);
			GameSleep(50);
			blt(384,(264+(i*16)),32,48,128,0,32,0,pdc);
			GameSleep(50);
		}
		MemDC.SelectObject(hbmp_1);//荻娜
		resetblt();
		for(i=0;i<6;i++){//移动
			blt(384,(256+(i*16)),32,48,160,0,64,0,pdc);
			GameSleep(50);
			blt(384,(264+(i*16)),32,48,128,0,32,0,pdc);
			GameSleep(50);
		}
		blt(384,320,32,48,96,96,0,96,pdc);
		GameSleep(500);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,21,0,0,24);
		st_dialog=41;
	}
	else if(st_dialog==41){//卡洛德冲上来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		for(i=0;i<8;i++){//移动
			blt(384,(480-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(50);
			blt(384,(472-(i*16)),32,48,128,96,32,96,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,41,0,0,25);
		st_dialog=42;
	}
	else if(st_dialog==42){
		Dialog(4,0,31,0,26);
		st_dialog=43;
	}
	else if(st_dialog==43){
		Dialog(4,41,0,0,27);
		st_dialog=44;
	}
	else if(st_dialog==44){
		Dialog(2,0,0,0,28);
		st_dialog=45;
	}
	else if(st_dialog==45){//卡诺使用圣剑
		lclose();
		StopMidiPlay();
		OpenMap("map\\maps103.bmp","map\\map109b.map","map\\map109b.npc");
		current.x=116;
		current.y=96;
		azimuth=9;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		processenemies();
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(370,202,48,48,96,336,0,336,&BuffDC);
		MemDC.SelectObject(hbmp_5);//圣剑
		resetblt();
		blt(390,160,32,48,96,48,0,48,&BuffDC);
		lopen();
		PlayWave("snd//move.wav");
		CDC *pdc=GetDC();
		blt(390,160,32,48,96,96,0,96,pdc);
		GameSleep(150);
		blt(390,160,32,48,128,96,32,96,pdc);
		GameSleep(150);
		blt(390,160,32,48,160,96,64,96,pdc);
		GameSleep(150);
		blt(390,160,32,48,96,144,0,144,pdc);
		GameSleep(150);
		blt(390,160,32,48,128,144,32,144,pdc);
		GameSleep(150);
		blt(390,160,32,48,160,144,64,144,pdc);
		GameSleep(150);
		//爆炸横扫屏幕的景象
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(370,202,48,48,96,336,0,336,&BuffDC);
		MemDC.SelectObject(hbmp_5);//圣剑
		resetblt();
		blt(390,160,32,48,96,48,0,48,&BuffDC);
		MemDC.SelectObject(hbmp_4);//爆炸
		for(i=0;i<8;i++){//移动
		PlayWave("snd\\bomb2.wav");
			for(int j=0;j<10;j++){
				for(int k=0;k<15;k++){
					pdc->BitBlt((k*54),(320-i*50),54,50,&BuffDC,(k*54),(320-i*50),SRCCOPY);
					pdc->BitBlt((k*54),(320-i*50),54,50,&MemDC,(j*54),50,SRCAND);
					pdc->BitBlt((k*54),(320-i*50),54,50,&MemDC,(j*54),0,SRCINVERT);
				}
				GameSleep(20);
			}
		pdc->BitBlt(0,(320-i*50),800,50,&BuffDC,0,(320-i*50),SRCCOPY);
		GameSleep(100);
		}
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(370,202,48,48,96,336,0,336,&BuffDC);
		MemDC.SelectObject(hbmp_5);//圣剑
		resetblt();
		blt(390,160,32,48,96,48,0,48,&BuffDC);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		GameSleep(100);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,36,0,48);
		st_dialog=46;
	}
	else if(st_dialog==46){//沙子
		lclose();
		PlayMidi("midi\\p1015.mid");
		current.x=53;
		current.y=70;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		lopen();
		Dialog(4,0,8,0,29);
		st_dialog=47;
	}
	else if(st_dialog==47){
		Dialog(4,0,6,0,30);
		st_dialog=48;
	}
	else if(st_dialog==48){
		Dialog(4,0,8,0,31);
		st_dialog=49;
	}
	else if(st_dialog==49){
		Dialog(4,46,0,0,32);
		st_dialog=50;
	}
	else if(st_dialog==50){
		Dialog(4,0,6,0,33);
		st_dialog=51;
	}
	else if(st_dialog==51){//移动到CIA处
		CDC *pdc=GetDC();
		for(i=0;i<32;i++){
			DrawMap();
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			current.y++;
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,14,0,0,34);
		st_dialog=52;
	}
	else if(st_dialog==52){
		Dialog(4,11,0,0,35);
		st_dialog=53;
	}
	else if(st_dialog==53){
		Dialog(4,11,0,0,36);
		st_dialog=54;
	}
	else if(st_dialog==54){//移动到司徒处
		CDC *pdc=GetDC();
		for(i=0;i<40;i++){
			DrawMap();
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			current.y-=2;
			current.x+=1;
			GameSleep(100);
		}
		for(i=0;i<10;i++){
			DrawMap();
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
			current.x+=2;
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,3,0,37);
		st_dialog=55;
	}
	else if(st_dialog==55){
		Dialog(4,0,4,0,38);
		st_dialog=56;
	}
	else if(st_dialog==56){
		Dialog(4,0,4,0,39);
		st_dialog=57;
	}
	else if(st_dialog==57){
		Dialog(4,0,4,0,40);
		st_dialog=58;
	}
	else if(st_dialog==58){
		Dialog(4,0,3,0,41);
		st_dialog=59;
	}
	else if(st_dialog==59){
		Dialog(4,0,3,0,42);
		st_dialog=60;
	}
	else if(st_dialog==60){
		Dialog(4,0,3,0,43);
		st_dialog=61;
	}
	else if(st_dialog==61){
		Dialog(4,0,1,0,44);
		st_dialog=62;
	}
	else if(st_dialog==62){
		Dialog(4,0,4,0,45);
		st_dialog=63;
	}
	else if(st_dialog==63){
		Dialog(4,0,2,0,46);
		st_dialog=64;
	}
	else if(st_dialog==64){
		Dialog(2,0,0,0,47);
		st_dialog=65;
	}
	else if(st_dialog==65){
		current.x=116;
		current.y=96;
		azimuth=9;
		movest=0;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(320,202,32,48,96,144,0,144,&BuffDC);
		MemDC.SelectObject(hbmp_5);//圣剑
		resetblt();
		blt(390,160,32,48,96,0,0,0,&BuffDC);
		MemDC.SelectObject(hbmp_1);//荻娜
		resetblt();
		blt(480,202,32,48,96,48,0,48,&BuffDC);
		lopen();
		Dialog(4,21,0,0,49);
		st_dialog=66;
	}
	else if(st_dialog==66){
		Dialog(4,0,32,0,50);
		st_dialog=67;
	}
	else if(st_dialog==67){//发生爆炸
		PlayWave("snd\\bomb2.wav");
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_4);//爆炸图片
		for(i=0;i<10;i++){
			pdc->BitBlt(200,250,400,200,&BuffDC,200,250,SRCCOPY);
			pdc->StretchBlt(350,150,108,100,&MemDC,(i*54),50,54,50,SRCAND);
			pdc->StretchBlt(350,150,108,100,&MemDC,(i*54),0,54,50,SRCINVERT);
			GameSleep(100);
		}
		BuffDC.BitBlt(200,150,400,200,&MapDC,200,150,SRCCOPY);
		pdc->BitBlt(200,150,400,200,&BuffDC,200,150,SRCCOPY);
		//卡诺和荻娜倒地
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(320,234,64,32,96,864,0,864,pdc);
		MemDC.SelectObject(hbmp_1);//荻娜
		resetblt();
		blt(480,202,32,48,160,192,64,192,pdc);
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		for(i=0;i<12;i++){//移动
			blt(608,(480-(i*16)),32,48,160,96,64,96,pdc);
			GameSleep(50);
			blt(608,(472-(i*16)),32,48,128,96,32,96,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,41,0,0,51);
		st_dialog=68;
	}
	else if(st_dialog==68){//沙子
		lclose();
		current.x=53;
		current.y=70;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		lopen();
		Dialog(3,0,8,0,52);
		st_dialog=69;
	}
	else if(st_dialog==69){
		Dialog(3,0,6,0,53);
		st_dialog=70;
	}
	else if(st_dialog==70){
		vclose();
		st_dialog=71;
		Dialog(5,0,0,0,55);
	}
	else if(st_dialog==71){
		st=8;//第七段
		st_sub1=0;
		st_dialog=0;
		Dialog(2,0,0,0,61);
		Para7Init();
	}
}//st_sub1=1的部分结束
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para6Accident(int type)
{
if(st_sub1==0){
	if((type==205)||(type==206)){//封闭通往106地图和上山的道路
		Dialog(4,0,31,0,8);
		st_dialog=21;
	}
	else if((type==210)||(type==212)){//禁止返回原地图
		Dialog(4,0,31,0,57);
		st_dialog=21;
	}
	else if(type==721){//禁止冒险冲锋
		Dialog(4,0,31,0,8);
		st_dialog=30;
	}
	else if(type==720){//西蒙看到潜艇
		Dialog(4,0,16,0,9);
		st_dialog=31;
	}
	else if(type==728){//西蒙要求调查潜艇，不许离开沙滩
		Dialog(4,0,16,0,59);
		st_dialog=21;
	}
	else if(type==213){//进入潜艇
		lclose();
		PlayMidi("midi\\p1017.mid");
		m_info_map=0;//关闭地图支持
		m_oldinfo_map=0;///////////
		current.x=140;
		current.y=23;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;
		lopen();
	}
	else if(type==214){//走出潜艇//important
		if(m_info_prop4==5){//如果得到手枪，就可以走了
			lclose();
			OpenMap("map\\maps101.bmp","map\\map101a.map","map\\map101b.npc");//调入地图
			m_info_map=1;//打开地图支持
			m_oldinfo_map=1;///////////
			base_x=190;//设定地图基准点
			base_y=380;///////////////
			current.x=30;
			current.y=119;
			st_sub1=1;//进入下一个阶段
			st_dialog=0;
			Dialog(5,0,0,0,55);//参数5表示存盘
		}
		else{
			Dialog(4,0,31,0,15);
			st_dialog=20;
		}
	}
	//以下用于处理地图切换
	else if(type==207){//地界g（地图103右方的道路）对应地界j
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map104b.map","map\\map104b.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=194;//设定地图基准点
		base_y=287;///////////////
		current.x=15;
		current.y=139;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4&&bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==211){//地界k（地图104下方的道路）对应地界l
		lclose();
		StopActTimer();
		OpenMap("map\\maps101.bmp","map\\map101a.map","map\\map101a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=380;///////////////
		current.x=65;
		current.y=18;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4&&bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	//察看潜艇中的壁画
	else if(type==762){
		Dialog(2,0,0,2,54);
		st_dialog=20;
	}
	else if(type==763){
		Dialog(2,0,0,3,54);
		st_dialog=20;
	}
	else if(type==764){
		Dialog(2,0,0,4,54);
		st_dialog=20;
	}
	else if(type==765){
		Dialog(2,0,0,5,54);
		st_dialog=20;
	}
	else if(type==766){
		Dialog(2,0,0,6,63);//像雅科夫列夫这样功勋卓著的设计师……
		st_dialog=20;
	}
	else if(type==767){
		Dialog(2,0,0,7,54);
		st_dialog=20;
	}
	else if(type==768){
		Dialog(2,0,0,8,54);
		st_dialog=20;
	}
	else if(type==769){
		Dialog(2,0,0,9,54);
		st_dialog=20;
	}
	//修改密码锁的密码
	else if(type==770){
		ChangeSN(107);
	}
	else if(type==771){
		ChangeSN(108);
	}
	else if(type==772){
		ChangeSN(109);
	}
	else if(type==773){
		ChangeSN(110);
	}
	else if(type==774){
		ChangeSN(111);
	}
	else if(type==775){
		ChangeSN(112);
	}
	else if(type==776){
		ChangeSN(113);
	}
	else if(type==777){
		ChangeSN(114);
	}
	//接触保险柜
	else if(type==761)
	{
		if ((map[104][29]==767)&&(m_info_prop4!=5))
		{//已经打开且没有手枪
			PlayWave("snd//prop.wav");
			GameSleep(500);
			Dialog(1,0,0,1,10);
			m_info_prop4=5;
			bullet=2000;//得到2000发子弹
			CDC *pdc=GetDC();
			RenewInfo(pdc);//在信息栏中显示物品小图
			ReleaseDC(pdc);
			pdc = NULL;
			st_dialog=32;
		}
		else
		{//还没有解开密码
			Dialog(2,0,0,0,62);
			st_dialog=20;
		}
	}
}//st_sub1=0的部分结束
if(st_sub1==1){
	if(type==213){//潜艇
		Dialog(4,26,0,0,16);
		st_dialog=21;
	}
	else if((type==211)||(type==224)||(type==221)){//禁止返回原地图
		Dialog(4,0,31,0,57);
		st_dialog=21;
	}
	else if((type==210)||(type==223)||(type==231)||(type==230)||(type==232)||(type==236)||(type==709))
	{//封闭地界
		Dialog(4,26,0,0,16);
		st_dialog=21;
	}
	else if(type==215){//断桥处
		Dialog(4,0,0,0,17);
		st_dialog=30;
	}
	else if(type==725){//到达防线//important
		Dialog(4,0,0,0,23);
		st_dialog=40;
	}
	//地图切换段
	else if(type==212){//地界l（地图103右方的道路）对应地界k
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map104b.map","map\\map104c.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=194;//设定地图基准点
		base_y=287;///////////////
		current.x=65;
		current.y=185;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==225){//地界y（地图104上方的道路）对应地界x
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map108d.map","map\\map108g.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=187;///////////////
		current.y=185;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.x=143;
		int offset_x=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.x+=offset_x;
			offset_x=(offset_x>0? -(offset_x+1) : -(offset_x-1));//offset_x不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==222){//地界v（地图108右上方的道路）对应地界u
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map109b.map","map\\map109b.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=187;///////////////
		current.x=15;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.y=53;
		int offset_y=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.y+=offset_y;
			offset_y=(offset_y>0? -(offset_y+1) : -(offset_y-1));//offset_y不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
}//st_sub1=1的部分结束
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para6Timer()
{
	processenemies();
}
//处理密码的程序段███████████████████████████████
void CMainFrame::ChangeSN(int x)
{
	int i;

	PlayWave("snd//ding.wav");
	int a=map[x][29];
	switch(a){
	case 561:a=562;break;
	case 562:a=563;break;
	case 563:a=564;break;
	case 564:a=565;break;
	case 565:a=601;break;
	case 601:a=602;break;
	case 602:a=603;break;
	case 603:a=604;break;
	case 604:a=605;break;
	case 605:a=561;break;
	}
	map[x][29]=a;
	map[104][29]=767;
	map[105][29]=768;//打开保险柜
	int sn[8]={563,565,605,603,561,604,602,601};
	for(i=107;i<115;i++){
		if(map[i][29]!=sn[(i-107)]){//只要有一个错了，就关闭保险柜。
			map[104][29]=687;
			map[105][29]=688;
		}
	}
	DrawMap();
	BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
	DrawActor(1);
	CDC *pdc=GetDC();
	pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
	ReleaseDC(pdc);
	pdc = NULL;
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████