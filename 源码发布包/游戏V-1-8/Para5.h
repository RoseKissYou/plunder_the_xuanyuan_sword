void CMainFrame::Para5Init(void)
{
/////////////////////////////////////////////////////////////////////////////
//调入所需对话，主角图片，地图文件群。
	LoadDialog("dlg\\para5.dlg");//调入对话
	LoadActor("pic\\hero.bmp");//调入主角
	deleteallbmpobjects();
/////////////////////////////////////////////////////////////////////////////
//调入所需头像
	hbmp_Photo0=(HBITMAP)LoadImage(NULL,"pic\\pic300.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_Photo1=(HBITMAP)LoadImage(NULL,"pic\\pic302.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic323.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡洛德
	hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic320.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//德里安
	hbmp_Photo4=(HBITMAP)LoadImage(NULL,"pic\\pic315.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（1）
	hbmp_Photo5=(HBITMAP)LoadImage(NULL,"pic\\pic309.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//哈德尔
	hbmp_Photo6=(HBITMAP)LoadImage(NULL,"pic\\pic310.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（1）
	hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic322.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//西蒙
	hbmp_Photo8=(HBITMAP)LoadImage(NULL,"pic\\pic321.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//洛桑
	hbmp_Photo9=(HBITMAP)LoadImage(NULL,"pic\\pic324.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//阿尔夫
	//在沙子落下之后，需要更换图片
/////////////////////////////////////////////////////////////////////////////
//调入所需道具
	::DeleteObject(hbmp_Prop0);//释放内存
	::DeleteObject(hbmp_Prop1);//释放内存
	::DeleteObject(hbmp_Prop2);//释放内存
/////////////////////////////////////////////////////////////////////////////
//调入其他图片
	hbmp_0=(HBITMAP)LoadImage(NULL,"pic\\pic107.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜
	hbmp_1=(HBITMAP)LoadImage(NULL,"pic\\pic017.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//火山
	hbmp_2=(HBITMAP)LoadImage(NULL,"pic\\pic100.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_3=(HBITMAP)LoadImage(NULL,"pic\\pic103.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic104.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//教徒
	hbmp_5=(HBITMAP)LoadImage(NULL,"pic\\hero.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺
	hbmp_6=(HBITMAP)LoadImage(NULL,"pic\\pic114.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//圣剑单图
	hbmp_7=(HBITMAP)LoadImage(NULL,"pic\\pic210.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//汗叹倒
	hbmp_8=(HBITMAP)LoadImage(NULL,"pic\\pic108.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//德里安
	hbmp_9=(HBITMAP)LoadImage(NULL,"pic\\pic112.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//军官
	::DeleteObject(hbmp_10);
	hbmp_11=(HBITMAP)LoadImage(NULL,"pic\\pic101.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_12=(HBITMAP)LoadImage(NULL,"pic\\pic102.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//CIA
	::DeleteObject(hbmp_13);
	hbmp_14=(HBITMAP)LoadImage(NULL,"pic\\pic115.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//士兵
	hbmp_15=(HBITMAP)LoadImage(NULL,"pic\\pic110.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//白莲教群体
	hbmp_16=(HBITMAP)LoadImage(NULL,"pic\\pic020.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//刀光剑影
	hbmp_17=(HBITMAP)LoadImage(NULL,"pic\\pic007.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//爆炸
	hbmp_18=(HBITMAP)LoadImage(NULL,"pic\\pic113.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//安妮
/////////////////////////////////////////////////////////////////////////////
//引入开始游戏的位置
/////////////////////////////////////////////////////////////////////////////
}//OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106a.npc");//调入地图的语句
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para5Info(int type)
{
	int i;

if(st_sub1==0){
	if(st_dialog==0)
	{
		PlayMidi("midi\\p1008.mid");
		blood=200;
		OpenMap("map\\maps103.bmp","map\\map108c.map","map\\map108f.npc");//调入地图的语句
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=187;///////////////
		current.x=97;
		current.y=154;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_DOWN;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		lopen();
		Dialog(2,0,0,0,97);
		st_progress=1;
		st_dialog=20;
	}
	else if(st_dialog==20)
	{//如果是自由行走状态，则转入此。
		m_info_st=8;
		m_oldinfo_st=8;
		SetActTimer();
		st_dialog=2000;
	}
	else if(st_dialog==21)
	{//如果是战斗状态，则转入此。
		if(m_info_prop4 && bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		SetActTimer();
		st_dialog=2000;
	}
	else if(st_dialog==50){//遇到哈德尔
		Dialog(4,21,0,0,15);
		st_dialog=20;
	}
	//以下是遇见村长的部分
	else if(st_dialog==101){//洛桑村长
		Dialog(4,41,0,0,17);
		st_dialog=102;
	}
	else if(st_dialog==102){
		Dialog(4,0,31,0,18);
		st_dialog=103;
	}
	else if(st_dialog==103){
		Dialog(4,41,0,0,19);
		st_dialog=104;
	}
	else if(st_dialog==104){
		Dialog(4,41,0,0,20);
		st_dialog=105;
	}
	else if(st_dialog==105){
		Dialog(4,0,31,0,21);
		OpenMap("map\\maps106.bmp","map\\map106b.map","map\\map106c.npc");//更换成没有村长的地图
		st_dialog=700;
	}//洛桑镇长的段落结束
	else if(st_dialog==201){//西蒙镇长
		Dialog(4,0,36,0,11);
		st_dialog=202;
	}
	else if(st_dialog==202){
		Dialog(4,21,0,0,12);
		st_dialog=203;
	}
	else if(st_dialog==203){
		Dialog(4,0,36,0,13);
		OpenMap("map\\maps103.bmp","map\\map103e.map","map\\map103f.npc");//更换成没有村长的地图
		st_dialog=700;
	}//西蒙镇长的段落结束
	else if(st_dialog==301){//阿尔夫镇长
		Dialog(4,0,46,0,3);
		st_dialog=302;
	}
	else if(st_dialog==302){
		Dialog(4,21,0,0,4);
		st_dialog=303;
	}
	else if(st_dialog==303){
		Dialog(4,0,46,0,5);
		st_dialog=304;
	}
	else if(st_dialog==304){
		Dialog(4,21,0,0,6);
		st_dialog=305;
	}
	else if(st_dialog==305){
		Dialog(4,0,31,0,7);
		st_dialog=306;
	}
	else if(st_dialog==306){
		Dialog(4,0,46,0,8);
		current.x=oldcurrent.x;
		current.y=oldcurrent.y;//恢复原来的位置
		st_dialog=700;
	}
	//切换场景的选择段
	else if(st_dialog==700){
		vclose();
		Dialog(2,0,0,0,9);
		if((st_progress==2)||(st_progress==3)||(st_progress==5))
		{//第一场
			st_dialog=400;
		}
		else if((st_progress==6)||(st_progress==10)||(st_progress==15))
		{//第二场
			st_dialog=500;
		}
		else if(st_progress==30)
		{//第三场
			st_dialog=600;//600段结束后，自然进入下一个段落。
		}
	}
	//司徒和沙子纠缠的三个场景
	else if(st_dialog==400){//第一场
		PlayMidi("midi\\p1011.mid");
		//切换地图
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		strcpy(oldfname01,fname01);
		strcpy(oldfname02,fname02);
		strcpy(oldfname03,fname03);//保存当前的地图
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		current.x=157;
		current.y=114;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		current.x=oldcurrent.x;
		current.y=oldcurrent.y;
		strcpy(fname01,oldfname01);
		strcpy(fname02,oldfname02);
		strcpy(fname03,oldfname03);//恢复地图
		OpenMap(fname01,fname02,fname03);//调入以前的地图
		//
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_2);//司徒
		resetblt();
		blt(516,304,32,32,64,160,0,160,&BuffDC);//司徒
		vopen();
		Dialog(2,0,0,0,22);
		st_dialog=401;
	}
	else if(st_dialog==401){
		Dialog(4,0,5,0,23);
		st_dialog=402;
	}
	else if(st_dialog==402){//沙子追上来
		MemDC.SelectObject(hbmp_15);//白莲教团
		resetblt();
		CDC *pdc=GetDC();
		for(i=0;i<12;i++)
		{
			blt((i*16),272,64,96,192,224,0,224,pdc);
			GameSleep(100);
			blt((8+(i*16)),272,64,96,256,224,64,224,pdc);
			GameSleep(100);
		}
		GameSleep(500);
		Dialog(4,6,0,0,24);
		st_dialog=403;
	}
	else if(st_dialog==403){//司徒汗ing……
		MemDC.SelectObject(hbmp_7);//汗叹倒
		CDC *pdc=GetDC();
		resetblt();
		blt(524,272,35,33,0,0,35,0,pdc);//汗
		GameSleep(500);
		resetblt();
		blt(524,272,35,33,0,0,70,0,pdc);//汗
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,3,0,25);
		st_dialog=404;
	}
	else if(st_dialog==404){//司徒转头，叹ing……
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//司徒
		resetblt();
		blt(516,304,32,32,64,96,0,96,pdc);//司徒
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(524,272,35,33,0,0,105,0,pdc);//叹
		GameSleep(500);
		resetblt();
		blt(524,272,35,33,0,0,140,0,pdc);//叹
		PlayWave("snd//whoa.wav");
		GameSleep(1000);
		blt(0,0,0,0,0,0,0,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,9,0,0,26);
		st_dialog=405;
	}
	else if(st_dialog==405){
		Dialog(3,0,3,0,27);
		st_dialog=406;
	}
	else if(st_dialog==406){
		Dialog(4,8,0,0,28);
		st_dialog=407;
	}
	else if(st_dialog==407){
		Dialog(4,0,4,0,29);
		st_dialog=408;
	}
	else if(st_dialog==408){//战斗
		CDC *pdc=GetDC();
		PlayWave("snd\\fight2.wav");
		oldlx=216;oldly=304;oldx=32;oldy=32;
		int k=0;
		int l=0;
		int m=0;
		int n=0;
		for(i=0;i<20;i++)
		{//司徒沙子向中间冲来。
			MemDC.SelectObject(hbmp_2);//司徒
			blt((516-(i*8)),304,32,32,64,96,0,96,pdc);//司徒
			k=oldlx;l=oldly;
			oldlx=m;oldly=n;
			MemDC.SelectObject(hbmp_3);//沙子
			blt((216+(i*8)),304,32,32,64,96,0,96,pdc);//沙子
			m=oldlx;n=oldly;
			oldlx=k;oldly=l;
			GameSleep(50);
		}
		k=304;
		l=0;
		for(i=0;i<28;i++)
		{//两人飞起来
			MemDC.SelectObject(hbmp_2);//司徒
			blt(366,k,32,32,64,96,0,96,pdc);//司徒
			GameSleep(50);
			k-=4;
			MemDC.SelectObject(hbmp_3);//沙子
			blt(346,k,32,32,64,96,0,96,pdc);//沙子
			GameSleep(50);
			k-=4;
			MemDC.SelectObject(hbmp_16);//刀光剑影
			blt(340,(k-16),64,64,l,64,l,0,pdc);//刀光剑影
			GameSleep(50);
			k-=4;
			if (l==320){l=0;}else{l+=64;}
		}
		l=0;
		for(i=0;i<39;i++)
		{//司徒沙子分别落下。
			MemDC.SelectObject(hbmp_2);//司徒
			blt(296,l,32,32,64,96,0,96,pdc);//司徒
			k=oldlx;l=oldly;
			oldlx=m;oldly=n;
			MemDC.SelectObject(hbmp_3);//沙子
			blt(416,l,32,32,64,96,0,96,pdc);//沙子
			m=oldlx;n=oldly;
			oldlx=k;oldly=l;
			GameSleep(50);
			l+=8;
		}
		//减血
		BuffDC.BitBlt(100,200,43,22,&InfoDC,243,108,SRCAND);
		BuffDC.BitBlt(100,200,43,22,&InfoDC,200,108,SRCINVERT);
		BuffDC.BitBlt(400,200,41,22,&InfoDC,327,108,SRCAND);
		BuffDC.BitBlt(400,200,41,22,&InfoDC,286,108,SRCINVERT);
		BuffDC.BitBlt(150,210,202,11,&InfoDC,189,19,SRCCOPY);
		BuffDC.StretchBlt(151,211,40,9,&InfoDC,746,87,1,9,SRCCOPY);
		BuffDC.BitBlt(450,210,202,11,&InfoDC,189,19,SRCCOPY);
		BuffDC.StretchBlt(451,211,190,9,&InfoDC,746,87,1,9,SRCCOPY);
		pdc->BitBlt(0,200,800,60,&BuffDC,0,200,SRCCOPY);//显示
		GameSleep(2000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(3,0,1,0,30);
		st_dialog=409;
	}
	else if(st_dialog==409){//加血
		PlayWave("snd\\prop.wav");
		CDC *pdc=GetDC();
		BuffDC.StretchBlt(151,211,60,9,&InfoDC,746,87,1,9,SRCCOPY);
		pdc->BitBlt(0,200,800,60,&BuffDC,0,200,SRCCOPY);//显示
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(2,0,0,0,32);
		st_dialog=410;
	}
	else if(st_dialog==410){//加血
		PlayWave("snd\\prop.wav");
		CDC *pdc=GetDC();
		BuffDC.StretchBlt(151,211,100,9,&InfoDC,746,87,1,9,SRCCOPY);
		pdc->BitBlt(0,200,800,60,&BuffDC,0,200,SRCCOPY);//显示
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(2,0,0,0,31);
		st_dialog=411;
	}
	else if(st_dialog==411){//加血
		PlayWave("snd\\prop.wav");
		CDC *pdc=GetDC();
		BuffDC.StretchBlt(151,211,200,9,&InfoDC,746,87,1,9,SRCCOPY);
		pdc->BitBlt(0,200,800,60,&BuffDC,0,200,SRCCOPY);//显示
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(1000);
		Dialog(2,0,0,0,33);
		st_dialog=412;
	}
	else if(st_dialog==412){//司徒逃跑
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_3);//沙子
		resetblt();
		blt(416,304,32,32,64,32,0,32,pdc);//沙子
		MemDC.SelectObject(hbmp_2);
		resetblt();
		for(i=0;i<16;i++)
		{//司徒向沙子冲去
			blt((296+(i*8)),304,32,32,64,160,0,160,pdc);
			GameSleep(50);
		}
		PlayWave("system\\enemies\\wound.wav");
		MemDC.SelectObject(hbmp_16);//刀光剑影
		pdc->BitBlt(400,288,64,64,&MemDC,192,64,SRCAND);
		pdc->BitBlt(400,288,64,64,&MemDC,192,0,SRCINVERT);
		GameSleep(50);
		pdc->BitBlt(400,288,64,64,&BuffDC,400,288,SRCCOPY);
		MemDC.SelectObject(hbmp_3);//沙子
		oldlx=416;oldly=304;oldx=32;oldy=32;
		blt(416,304,32,32,64,96,0,96,pdc);//沙子
		resetblt();
		MemDC.SelectObject(hbmp_2);
		for(i=0;i<25;i++)
		{//司徒越过沙子逃跑
			blt((448+(i*16)),304,32,32,64,160,0,160,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,8,0,0,34);
		st_dialog=413;
	}
	else if(st_dialog==413){
		vclose();
		m_info_st=8;
		m_oldinfo_st=8;
		PlayMidi("midi\\p1008.mid");
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		SetActTimer();
		st_dialog=2000;
	}//第一场结束
	else if(st_dialog==500){//第二场
		PlayMidi("midi\\p1011.mid");
		//切换地图
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		strcpy(oldfname01,fname01);
		strcpy(oldfname02,fname02);
		strcpy(oldfname03,fname03);//保存当前的地图
		OpenMap("map\\maps103.bmp","map\\map105a.map","map\\map105a.npc");//调入地图
		current.x=138;
		current.y=40;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		current.x=oldcurrent.x;
		current.y=oldcurrent.y;
		strcpy(fname01,oldfname01);
		strcpy(fname02,oldfname02);
		strcpy(fname03,oldfname03);//恢复地图
		OpenMap(fname01,fname02,fname03);//调入以前的地图
		//
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_2);//司徒
		resetblt();
		blt(466,250,32,32,64,96,0,96,&BuffDC);
		MemDC.SelectObject(hbmp_15);//白莲教群体
		resetblt();
		blt(288,218,64,96,192,224,0,224,&BuffDC);
		vopen();
		Dialog(4,8,0,0,35);
		st_dialog=501;
	}
	else if(st_dialog==501){
		Dialog(4,0,3,0,36);
		st_dialog=502;
	}
	else if(st_dialog==502){//士兵围上来
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_9);//军官
		resetblt();
		for(i=0;i<10;i++)
		{
			blt(320,(i*16),32,48,128,0,64,0,pdc);
			GameSleep(50);
			blt(320,(8+(i*16)),32,48,96,0,32,0,pdc);
			GameSleep(50);
		}
		GameSleep(1000);
		MemDC.SelectObject(hbmp_14);//士兵
		for(int j=352;j<460;j+=32)
		{
			resetblt();
			for(i=0;i<10;i++)
			{
				blt(j,(i*16),32,48,128,0,64,0,pdc);
				GameSleep(50);
				blt(j,(8+(i*16)),32,48,96,0,32,0,pdc);
				GameSleep(50);
			}
			resetblt();
			for(i=0;i<10;i++)
			{
				blt(j,(480-(i*16)),32,48,128,96,64,96,pdc);
				GameSleep(50);
				blt(j,(472-(i*16)),32,48,96,96,32,96,pdc);
				GameSleep(50);
			}
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,0,0,37);
		st_dialog=503;
	}
	else if(st_dialog==503){
		Dialog(4,0,0,0,38);
		st_dialog=504;
	}
	else if(st_dialog==504){
		Dialog(4,0,0,0,39);
		st_dialog=505;
	}
	else if(st_dialog==505){
		MemDC.SelectObject(hbmp_7);//汗叹倒
		CDC *pdc=GetDC();
		resetblt();
		blt(466,218,35,33,0,0,35,0,pdc);//汗
		GameSleep(500);
		resetblt();
		blt(466,218,35,33,0,0,70,0,pdc);//汗
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,3,0,40);
		st_dialog=506;
	}
	else if(st_dialog==506){//沙子汗ing……
		MemDC.SelectObject(hbmp_7);//汗叹倒
		CDC *pdc=GetDC();
		resetblt();
		blt(320,218,35,33,0,0,35,0,pdc);//汗
		GameSleep(500);
		resetblt();
		blt(320,218,35,33,0,0,70,0,pdc);//汗
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,8,0,0,41);
		st_dialog=507;
	}
	else if(st_dialog==507){//集体冒出省略号，延时。
		MemDC.SelectObject(hbmp_7);//汗叹倒
		CDC *pdc=GetDC();
		resetblt();
		blt(320,186,35,33,0,0,105,0,pdc);//!
		resetblt();
		blt(320,218,35,33,0,0,105,0,pdc);//!
		resetblt();
		blt(288,218,35,33,0,0,105,0,pdc);//!
		resetblt();
		blt(320,250,35,33,0,0,105,0,pdc);//!
		resetblt();
		blt(466,218,35,33,0,0,105,0,pdc);//!
		GameSleep(500);
		resetblt();
		blt(320,186,35,33,0,0,210,0,pdc);//…
		resetblt();
		blt(320,218,35,33,0,0,210,0,pdc);//…
		resetblt();
		blt(288,218,35,33,0,0,210,0,pdc);//…
		resetblt();
		blt(320,250,35,33,0,0,210,0,pdc);//…
		resetblt();
		blt(466,218,35,33,0,0,210,0,pdc);//…
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(2000);
		Dialog(4,8,0,0,42);
		st_dialog=508;
	}
	else if(st_dialog==508){
		vclose();
		m_info_st=8;
		m_oldinfo_st=8;
		PlayMidi("midi\\p1008.mid");
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		vopen();
		SetActTimer();
		st_dialog=2000;
	}//第二场结束
	else if(st_dialog==600){//第三场（可以调用地图）
		PlayMidi("midi\\p1011.mid");
		OpenMap("map\\maps103.bmp","map\\map109a.map","map\\map109a.npc");//调入地图
		current.x=114;
		current.y=21;
		azimuth=0;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_2);//司徒
		resetblt();
		blt(416,256,32,32,64,128,0,128,&BuffDC);
		MemDC.SelectObject(hbmp_15);//白莲教群体
		resetblt();
		blt(384,96,96,64,192,0,0,0,&BuffDC);
		MemDC.SelectObject(hbmp_9);//军官
		resetblt();
		blt(320,416,32,48,96,96,0,96,&BuffDC);
		vopen();
		GameSleep(1000);
		Dialog(4,0,0,0,43);
		st_dialog=601;
	}
	else if(st_dialog==601){//军官走
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_9);//军官
		resetblt();
		for(i=0;i<24;i++)
		{
			blt((320-(i*16)),416,32,48,160,48,64,48,pdc);
			GameSleep(50);
			blt((312-(i*16)),416,32,48,128,48,32,48,pdc);
			GameSleep(50);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,9,0,0,44);
		st_dialog=602;
	}
	else if(st_dialog==602){
		Dialog(4,0,3,0,45);
		st_dialog=603;
	}
	else if(st_dialog==603){
		Dialog(4,9,0,0,46);
		st_dialog=604;
	}
	else if(st_dialog==604){
		Dialog(4,0,5,0,47);
		st_dialog=605;
	}
	else if(st_dialog==605){
		Dialog(4,8,0,0,48);
		st_dialog=606;
	}
	else if(st_dialog==606){
		Dialog(4,0,5,0,49);
		st_dialog=607;
	}
	else if(st_dialog==607){
		Dialog(4,6,0,0,50);
		st_dialog=608;
	}
	else if(st_dialog==608){
		Dialog(4,0,5,0,51);
		st_dialog=609;
	}
	else if(st_dialog==609){
		Dialog(4,6,0,0,52);
		st_dialog=610;
	}
	else if(st_dialog==610){
		Dialog(4,0,5,0,53);
		st_dialog=611;
	}
	else if(st_dialog==611){
		Dialog(4,8,0,0,54);
		st_dialog=612;
	}
	else if(st_dialog==612){
		Dialog(4,0,3,0,55);
		st_dialog=613;
	}
	else if(st_dialog==613){
		Dialog(4,9,0,0,56);
		st_dialog=614;
	}
	else if(st_dialog==614){
		Dialog(4,0,3,0,57);
		st_dialog=615;
	}
	else if(st_dialog==615){
		Dialog(4,9,0,0,58);
		st_dialog=616;
	}
	else if(st_dialog==616){
		Dialog(4,0,5,0,59);
		st_dialog=617;
	}
	else if(st_dialog==617){
		Dialog(4,6,0,0,60);
		st_dialog=620;
	}
	else if(st_dialog==620){//沙子夺取昆仑镜
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_3);//沙子
		resetblt();
		for(i=0;i<13;i++)
		{
			blt(416,(128+(i*8)),32,32,64,0,0,0,pdc);
			GameSleep(50);
		}
		for(i=0;i<13;i++)
		{
			blt(416,(224-(i*8)),32,32,64,0,0,0,pdc);
			GameSleep(50);
		}
		MemDC.SelectObject(hbmp_15);//白莲教群体
		blt(384,96,96,64,192,0,0,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,61);
		st_dialog=621;
	}
	else if(st_dialog==621){
		Dialog(4,6,0,0,62);
		st_dialog=622;
	}
	else if(st_dialog==622){//司徒倒
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_2);//司徒
		oldlx=416;oldly=256;oldx=32;oldy=32;
		blt(416,256,64,32,64,32,0,32,pdc);
		PlayWave("snd//whoa.wav");
		GameSleep(1000);
		MemDC.SelectObject(hbmp_7);//汗叹倒
		resetblt();
		blt(464,230,35,33,0,0,175,0,pdc);//倒
		PlayWave("snd//whoa.wav");
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,0,5,0,63);
		st_dialog=623;
	}
	else if(st_dialog==623){
		Dialog(4,9,0,0,66);
		st_dialog=624;
	}
	else if(st_dialog==624){//沙子消失
		PlayWave("snd//move.wav");
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_15);//白莲教群体
		pdc->BitBlt(384,96,96,64,&MemDC,192,0,SRCAND);
		GameSleep(100);
		BuffDC.BitBlt(384,96,96,64,&MapDC,384,96,SRCCOPY);
		for(i=0;i<50;i++){
			pdc->BitBlt((384+i),96,1,64,&BuffDC,(384+i),96,SRCCOPY);
			pdc->BitBlt((480-i),96,1,64,&BuffDC,(480-i),96,SRCCOPY);
			GameSleep(20);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(3,0,0,0,65);
		st_dialog=630;
	}
	else if(st_dialog==630){//转入山洞的场景
		vclose();
		PlayMidi("midi\\p1017.mid");
		OpenMap("map\\maps102.bmp","map\\map102a.map","map\\map102a.npc");//调入地图
		current.x=98;
		current.y=32;
		azimuth=0;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_8);//老头
		resetblt();
		blt(328,360,32,48,96,96,0,96,&BuffDC);
		resetblt();
		blt(360,360,32,48,96,96,0,96,&BuffDC);
		resetblt();
		blt(392,360,32,48,96,96,0,96,&BuffDC);
		resetblt();
		blt(424,360,32,48,96,96,0,96,&BuffDC);
		resetblt();
		blt(456,360,32,48,96,96,0,96,&BuffDC);
		vopen();
		Dialog(2,0,0,0,67);
		st_dialog=631;
	}
	else if(st_dialog==631){
		Dialog(4,0,36,0,68);
		st_dialog=632;
	}
	else if(st_dialog==632){
		Dialog(4,0,36,0,69);
		st_dialog=633;
	}
	else if(st_dialog==633){
		Dialog(4,0,36,0,70);
		st_dialog=634;
	}
	else if(st_dialog==634){
		Dialog(4,0,46,0,71);
		st_dialog=635;
	}
	else if(st_dialog==635){
		Dialog(4,0,41,0,72);
		st_dialog=636;
	}
	else if(st_dialog==636){
		Dialog(4,16,0,0,73);
		st_dialog=637;
	}
	else if(st_dialog==637){
		Dialog(4,0,36,0,74);
		st_dialog=638;
	}
	else if(st_dialog==638){
		Dialog(4,16,0,0,75);
		st_dialog=639;
	}
	else if(st_dialog==639){
		Dialog(4,11,0,0,76);
		st_dialog=640;
	}
	else if(st_dialog==640){
		Dialog(4,16,0,0,77);
		st_dialog=641;
	}
	else if(st_dialog==641){
		Dialog(4,0,36,0,78);
		st_dialog=642;
	}
	else if(st_dialog==642){
		Dialog(4,11,0,0,79);
		st_dialog=643;
	}
	else if(st_dialog==643){
		Dialog(4,16,0,0,80);
		st_dialog=644;
	}
	else if(st_dialog==644){
		Dialog(4,0,46,0,81);
		st_dialog=645;
	}
	else if(st_dialog==645){
		Dialog(4,0,41,0,82);
		st_dialog=646;
	}
	else if(st_dialog==646){
		Dialog(4,0,36,0,83);
		st_dialog=647;
	}
	else if(st_dialog==647){//地震
		quake();
		Dialog(4,0,36,0,84);
		st_dialog=648;
	}
	else if(st_dialog==648){
		Dialog(4,0,46,0,85);
		st_dialog=649;
	}
	else if(st_dialog==649){
		Dialog(4,16,0,0,86);
		st_dialog=650;
	}
	else if(st_dialog==650){//地震
		quake();
		Dialog(4,11,0,0,87);
		st_dialog=651;
	}
	else if(st_dialog==651){//走人，地震
		MemDC.SelectObject(hbmp_8);//老头
		CDC *pdc=GetDC();
		for(int j=328;j<460;j+=32)
		{
			resetblt();
			for(i=0;i<10;i++)
			{
				blt(j,(360+(i*16)),32,48,128,0,64,0,pdc);
				GameSleep(50);
				blt(j,(368+(i*16)),32,48,96,0,32,0,pdc);
				GameSleep(50);
			}
		}
		quake();
		vclose();
		MemDC.SelectObject(hbmp_1);//火山的图片
		MapDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		MapDC.BitBlt(200,0,400,480,&MemDC,0,0,SRCCOPY);
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		BuffDC.BitBlt(200,0,400,480,&MemDC,0,0,SRCCOPY);
		lopen();
		quake();
		MemDC.SelectObject(hbmp_17);//爆炸图片
		for(i=0;i<10;i++){
			BuffDC.BitBlt(200,0,400,200,&MapDC,200,0,SRCCOPY);
			BuffDC.StretchBlt(336,96,108,100,&MemDC,(i*54),50,54,50,SRCAND);
			BuffDC.StretchBlt(336,96,108,100,&MemDC,(i*54),0,54,50,SRCINVERT);
			pdc->BitBlt(200,0,400,200,&BuffDC,200,0,SRCCOPY);
			GameSleep(100);
		}
		BuffDC.BitBlt(200,0,400,200,&MapDC,200,0,SRCCOPY);
		pdc->BitBlt(200,0,400,200,&BuffDC,200,0,SRCCOPY);
		GameSleep(1000);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,98);
		st_dialog=652;
	}

	else if(st_dialog==652){
		lclose();
		//更换部分头像
		hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic307.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫洛
		hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic308.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//安妮
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		current.x=138;
		current.y=62;
		movest=0;
		azimuth=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_9);//剑圣
		resetblt();
		blt(415,217,32,48,96,96,0,96,&BuffDC);//剑圣背影
		MemDC.SelectObject(hbmp_18);//安妮
		resetblt();
		blt(383,217,32,48,96,96,0,96,&BuffDC);//安妮背影
		vopen();
		Dialog(4,0,11,0,88);
		st_dialog=653;
	}
	else if(st_dialog==653){
		Dialog(4,16,0,0,89);
		st_dialog=654;
	}
	else if(st_dialog==654){
		vclose();
		st_dialog=655;
		Dialog(5,0,0,0,90);//参数5表示存盘
	}
	else if(st_dialog==655){
		st=7;//第六段
		st_sub1=0;
		st_dialog=0;
		Dialog(2,0,0,0,108);
		Para6Init();
	}
}//st_sub1==0的部分结束
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para5Accident(int type)
{
if(st_sub1==0){
	if(type==223){//火山
		StopActTimer();
		lclose();
		MemDC.SelectObject(hbmp_1);//火山的图片
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		BuffDC.BitBlt(200,0,400,480,&MemDC,0,0,SRCCOPY);
		lopen();
		Dialog(2,0,0,0,98);
		st_dialog=20;
	}
	else if(type==174){//哈德尔
		Dialog(4,0,26,0,14);
		st_dialog=50;
	}
	else if(type==186){//望山镇的村民
		Dialog(4,0,0,0,99);
		st_dialog=20;
	}
	else if(type==171){//西镇的村民
		Dialog(4,0,0,0,101);
		st_dialog=20;
	}
	else if(type==168){//西镇的卫兵
		Dialog(4,0,0,0,102);
		st_dialog=20;
	}
	else if(type==175){//大港镇的村民
		Dialog(4,0,0,0,101);
		st_dialog=20;
	}
	else if(type==176){//大港镇的卫兵
		Dialog(4,0,0,0,102);
		st_dialog=20;
	}
	else if(type==710){//中央镇的村民
		Dialog(4,0,0,0,92);
		st_dialog=20;
	}
	else if(type==711){//中央镇的村民
		Dialog(4,0,0,0,93);
		st_dialog=20;
	}
	else if(type==712){//中央镇的村民
		Dialog(4,0,0,0,94);
		st_dialog=20;
	}
	else if(type==713){//中央镇的村民
		Dialog(4,0,0,0,95);
		st_dialog=20;
	}
	else if(type==714){//中央镇的村民
		Dialog(4,0,0,0,96);
		st_dialog=20;
	}
	else if(type==715){//中央镇的村民
		Dialog(4,0,0,0,1);
		st_dialog=20;
	}
	else if(type==718){//中央镇的村民
		Dialog(4,0,0,0,105);
		st_dialog=20;
	}
	else if(type==719){//中央镇的村民
		Dialog(4,0,0,0,106);
		st_dialog=20;
	}
	//以下用于处理镇长的对话和插入片断部分
	else if(type==170){//洛桑2
		if((st_progress%2)!=0){st_progress*=2;}
		Dialog(4,0,31,0,16);
		st_dialog=101;
	}
	else if(type==178){//西蒙3
		if((st_progress%3)!=0){st_progress*=3;}
		Dialog(4,21,0,0,10);
		st_dialog=201;
	}
	else if(type==716){//阿尔夫办公室门口的卫兵5
		if((st_progress%5)!=0){
			StopActTimer();
			lclose();
			if((st_progress%5)!=0){st_progress*=5;}
			//移动场景
			CDC *pdc=GetDC();
			oldcurrent.x=current.x;
			oldcurrent.y=current.y;//备份
			current.x=126;
			current.y=103;
			azimuth= AZIMUTH_UP;
			movest=0;
			DrawMap();//准备地图
			BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
			DrawActor(1);
			MemDC.SelectObject(hbmp_0);//荻娜
			resetblt();
			blt(320,218,32,48,96,96,0,96,pdc);//荻娜背影
			ReleaseDC(pdc);
			pdc = NULL;
			//
			lopen();
			Dialog(4,21,0,0,2);
			st_dialog=301;
		}
		else{//镇长已经走了
			Dialog(4,0,0,0,104);
			st_dialog=20;
		}
	}
	//以下用于封闭地界
	else if(type==236){//山洞口
		Dialog(4,0,31,0,103);
		st_dialog=20;
	}
	else if(type==706){//墓地
		Dialog(4,0,31,0,100);
		st_dialog=21;
	}
	else if(type==709){//监狱
		Dialog(4,21,0,0,107);
		st_dialog=20;
	}
	else if(type==211){//地界k
		Dialog(4,0,31,0,103);
		st_dialog=20;
	}
	else if(type==232){//地界木
		Dialog(4,0,31,0,103);
		st_dialog=20;
	}
	else if(type==206){//地界f
		Dialog(4,0,31,0,103);
		st_dialog=20;
	}
	//以下是地图切换段（只有107一部分是战斗地图）
	else if(type==201){//地界A（地图106右上的道路）对应地界甲
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=187;///////////////
		current.x=15;
		current.y=40;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4&&bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==227){//地界甲（地图107左上的道路）对应地界A
		lclose();
		StopActTimer();
		if((st_progress%2)!=0){//调用有村长的地图
			OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106b.npc");//调入地图
		}
		else{//无村长的地图
			OpenMap("map\\maps106.bmp","map\\map106b.map","map\\map106c.npc");//调入地图
		}
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=0;//设定地图基准点
		base_y=187;///////////////
		current.x=180;
		current.y=40;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		SetActTimer();
	}
	else if(type==202){//地界B（地图106右下方的道路）对应地界C
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=187;///////////////
		current.x=15;
		current.y=114;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==203){//地界C（地图107左下方的道路）对应地界B
		lclose();
		StopActTimer();
		if((st_progress%2)!=0){//调用有村长的地图
			OpenMap("map\\maps106.bmp","map\\map106a.map","map\\map106b.npc");//调入地图
		}
		else{//无村长的地图
			OpenMap("map\\maps106.bmp","map\\map106b.map","map\\map106c.npc");//调入地图
		}
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=0;//设定地图基准点
		base_y=187;///////////////
		current.x=180;
		current.y=114;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==224){//地界X（地图108下方的道路）对应地界Y
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map104a.map","map\\map104a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=194;//设定地图基准点
		base_y=287;///////////////
		current.y=15;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.x=143;
		int offset_x=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.x+=offset_x;
			offset_x=(offset_x>0? -(offset_x+1) : -(offset_x-1));//offset_x不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==225){//地界y（地图104上方的道路）对应地界x
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map108c.map","map\\map108f.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=187;///////////////
		current.y=185;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.x=143;
		int offset_x=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.x+=offset_x;
			offset_x=(offset_x>0? -(offset_x+1) : -(offset_x-1));//offset_x不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==210){//地界J（地图104左方的道路）对应地界G
		lclose();
		StopActTimer();
		if((st_progress%3)!=0){//调用有村长的地图
			OpenMap("map\\maps103.bmp","map\\map103c.map","map\\map103d.npc");//调入地图
		}
		else{//无村长的地图
			OpenMap("map\\maps103.bmp","map\\map103e.map","map\\map103f.npc");//调入地图
		}
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=287;///////////////
		current.x=180;
		current.y=139;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==207){//地界g（地图103右方的道路）对应地界j
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map104a.map","map\\map104a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=194;//设定地图基准点
		base_y=287;///////////////
		current.x=15;
		current.y=139;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==204){//地界D（地图107正下方的道路）对应地界E
		lclose();
		StopActTimer();
		if((st_progress%3)!=0){//调用有村长的地图
			OpenMap("map\\maps103.bmp","map\\map103c.map","map\\map103d.npc");//调入地图
		}
		else{//无村长的地图
			OpenMap("map\\maps103.bmp","map\\map103e.map","map\\map103f.npc");//调入地图
		}
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=287;///////////////
		current.x=70;
		current.y=15;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==205){//地界E（地图103正上方的道路）对应地界D
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=187;///////////////
		current.x=70;
		current.y=185;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==231){//地界金（地图108左下的道路）对应地界丙
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map107a.map","map\\map107a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=94;//设定地图基准点
		base_y=187;///////////////
		current.x=178;
		current.y=143;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		if(m_info_prop4&&bullet>0){//战斗态，有枪优先用枪
			m_info_st=100;m_oldinfo_st=100;
		}else{
			m_info_st=10;m_oldinfo_st=10;
		}
		lopen();
		SetActTimer();
	}
	else if(type==229){//地界丙（地图107右下方的道路）对应地界金
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map108c.map","map\\map108f.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=187;///////////////
		current.x=18;
		current.y=143;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==222){//地界v（地图108右上方的道路）对应地界u
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map109a.map","map\\map109a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=187;///////////////
		current.x=15;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.y=53;
		int offset_y=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.y+=offset_y;
			offset_y=(offset_y>0? -(offset_y+1) : -(offset_y-1));//offset_y不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==221){//地界u（地图109左方的两条道路）对应108的地界v
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map108c.map","map\\map108f.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=187;///////////////
		current.x=180;
		//存在两个入口，判断一下最近的可行走各自，避免被卡在不可行走区域里面//current.y=53;
		int offset_y=1;
		while(map[current.x][current.y]>200){//地图格子，1-200范围是可以行走的
			current.y+=offset_y;
			offset_y=(offset_y>0? -(offset_y+1) : -(offset_y-1));//offset_y不断变换符号
		}
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if((type==217)||(type==218)){//地界q和地界r（地图105上方的道路）对应地界t和地界s
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map109a.map","map\\map109a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=187;///////////////
		current.y=185;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if((type==219)||(type==220)){//地界t和地界s（地图105上方的道路）对应地界q和地界r
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map105a.map","map\\map105a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=287;///////////////
		current.y=15;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==215){//地界o（地图104右方的道路）对应地界p
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map105a.map","map\\map105a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=290;//设定地图基准点
		base_y=287;///////////////
		current.x=15;
		current.y=162;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
	else if(type==216){//地界p（地图105左方的道路）对应地界o
		lclose();
		StopActTimer();
		OpenMap("map\\maps103.bmp","map\\map104a.map","map\\map104a.npc");//调入地图
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=194;//设定地图基准点
		base_y=287;///////////////
		current.x=180;
		current.y=162;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		m_info_st=8;//自由行走态
		m_oldinfo_st=8;
		lopen();
		SetActTimer();
	}
}//st_sub1==0的部分结束
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para5Timer()
{
	processenemies();
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████